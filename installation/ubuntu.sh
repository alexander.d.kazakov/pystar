## Add sourses (if necessary)
#sudo add-apt-repository main
#sudo add-apt-repository universe
#sudo add-apt-repository restricted
#sudo add-apt-repository multiverse

sudo apt-get install -y \
    apt-utils \
    cmake \
    build-essential \
    openmpi-bin \
    libfftw3-dev \
    libboost-dev libboost-serialization-dev libboost-mpi-dev libboost-filesystem-dev libboost-test-dev \
    cython3 python3 python3-numpy python3-h5py python3-opengl python3-matplotlib python3-pyqt5 python3-pyqtgraph \
    python3-pip\
    lcov \
    git \
    pycodestyle pylint3 \
    python3-vtk7 \
    libhdf5-openmpi-dev \
    libhdf5-openmpi-100:amd64 \
    libhdf5-100:amd64 \
    vim \
    ccache \
    curl

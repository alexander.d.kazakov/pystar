        self.check_savingdata.stateChanged.connect(self.enable_savingparameters)
        self.gravity_check.stateChanged['int'].connect(self.enable_gravityslider)
        self.thermostat_check.stateChanged.connect(self.thermostat_change)
        self.value_sigma.valueChanged.connect(self.lj_slider_connection_sig)
        self.value_epsilon.valueChanged.connect(self.lj_slider_connection_eps)
        self.value_epsilon.valueChanged.connect(self.reduce_temperature)
        self.epsilon_slider.valueChanged.connect(self.lj_value_connection_eps)
        self.sigma_slider.valueChanged.connect(self.lj_value_connection_sig)
        self.grav_red_unit.setVisible(False)
        self.temperature_change.valueChanged.connect(self.reduce_temperature)
        self.gravity_value.valueChanged.connect(self.grav_slider_connection)
        self.gravity_slide.valueChanged.connect(self.grav_value_connection)

    def enable_savingparameters(self, state):
        """enables groupbox for saving parameter options, when checkBox is true
        """
        if state > 0:
           self.groupbox_saveparameters.setEnabled(True)
        else:
           self.groupbox_saveparameters.setEnabled(False)

    def reduce_temperature(self):
        """contributes the temperature in reduced unit
        """
        self.temp_red_unit.setValue(float(self.temperature_change.value()/self.value_epsilon.value()))

    def enable_gravityslider(self, state):
        """shows gravity slider and spinbox, when checkBox is true
        """
        if state > 0:
            self.gravity_slide.setVisible(True)
            self.gravity_value.setVisible(True)
            self.label_red_grav.setVisible(True)
            self.grav_red_unit.setVisible(True)
        else:
            self.gravity_slide.setVisible(False)
            self.gravity_value.setVisible(False)
            self.label_red_grav.setVisible(False)
            self.grav_red_unit.setVisible(False)

    def grav_value_connection(self):
        self.gravity_value.setValue(self.gravity_slide.value()*0.01)
        self.grav_red_unit.setValue(self.gravity_value.value()/2.477709)

    def grav_slider_connection(self):
        self.gravity_slide.setValue(int(self.gravity_value.value()*100))
        self.grav_red_unit.setValue(self.gravity_value.value()/2.477709)

    def lj_slider_connection_eps(self):
        """ changes value of slider, when epsilon value is changed
        """
        self.epsilon_slider.setValue(int(self.value_epsilon.value()))

    def lj_slider_connection_sig(self):
        """ changes value of slider, when sigma is changed
        """
        self.sigma_slider.setValue(int(100*self.value_sigma.value()))

    def lj_value_connection_eps(self):
        """ changes value of epsilon, when its slider is changed
        """
        self.value_epsilon.setValue(self.epsilon_slider.value())

    def lj_value_connection_sig(self):
        """ changes value of sigma, when its slider is changed
        """
        self.value_sigma.setValue(0.01*self.sigma_slider.value())


    def thermostat_change(self, state):
        """enables switching temperature, when thermostat is turned off
        """
        if state > 0:
            self.temperature_change.setVisible(True)
            self.temperature_slide.setVisible(True)
            self.temperature_slide.setEnabled(True)

        else:
            self.temperature_change.setVisible(False)
            self.temperature_slide.setEnabled(False)

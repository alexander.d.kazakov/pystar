        self.check_savingdata.stateChanged.connect(self.enable_savingparameters)
        self.particles_np.valueChanged.connect(self.contribute_boxlength)
        self.particles_density.valueChanged.connect(self.contribute_boxlength)
        self.temperature_change.valueChanged.connect(self.reduce_temperature)

    def reduce_temperature(self):
        """contributes the temperature in reduced unit
        """
        self.temp_red_unit.setValue(float(self.temperature_change.value()/self.value_epsilon.value()))

    def contribute_boxlength(self):
        """contributes boxlength by given number of particles and density
        """
        volume = float(self.particles_np.value() / self.particles_density.value())
        self.box_volume_auto.setText(str(round(volume,3)))
        box_l = str(round(volume**(1/3), 3))
        self.box_length_auto.setText(box_l+","+ box_l+","+ box_l)

    def enable_savingparameters(self, state):
        """enables groupbox for saving parameter options, when checkBox is true
        """
        if state > 0:
           self.groupbox_saveparameters.setEnabled(True)
        else:
           self.groupbox_saveparameters.setEnabled(False)


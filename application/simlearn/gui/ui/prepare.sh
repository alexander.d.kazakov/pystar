# Let it be for a while like this
# TODO: rewrite it within python contexts

LJ2D_tools="./LJ2D/tools.py"
LJ3D_tools="./LJ3D/tools.py"
pV_tools="./pV/tools.py"
star_tools="./star/tools.py"

pyrcc5 -o resource_rc.py resources/resource.qrc
pyuic5 --from-imports -o welcome.py welcome/welcome.ui

pyuic5 --from-imports -o LJ2D.py LJ2D/LJ2D.ui
sed -i "/connectSlotsByName/r $LJ2D_tools" "LJ2D.py"
sed -i "/self.gravity_slide.setObjectName/a \
    \ \ \ \ \ \ \ \ self.gravity_slide.setVisible(False)" "LJ2D.py"
sed -i "/self.gravity_value.setObjectName/a \
    \ \ \ \ \ \ \ \ self.gravity_value.setVisible(False)" "LJ2D.py"

pyuic5 --from-imports -o LJ3D.py LJ3D/LJ3D.ui
sed -i "/connectSlotsByName/r $LJ3D_tools" "LJ3D.py"
sed -i "/self.gravity_slide.setObjectName/a \
    \ \ \ \ \ \ \ \ self.gravity_slide.setVisible(False)" "LJ3D.py"
sed -i "/self.gravity_value.setObjectName/a \
    \ \ \ \ \ \ \ \ self.gravity_value.setVisible(False)" "LJ3D.py"

pyuic5 --from-imports -o pV.py pV/pV.ui
sed -i "/connectSlotsByName/r $pV_tools" "pV.py"

pyuic5 --from-imports -o star.py star/star.ui
sed -i "/connectSlotsByName/r $star_tools" "star.py"

        self.check_savingdata.stateChanged.connect(self.enable_savingparameters)
        self.particles_np.valueChanged.connect(self.contribute_boxlength)
        self.particles_density.valueChanged.connect(self.contribute_boxlength)
        self.particles_density.valueChanged.connect(self.connect_red_density)
        self.red_density.valueChanged.connect(self.connect_density)
        self.gravity_check.stateChanged['int'].connect(self.enable_gravityslider)
        self.radiobutton_argon.toggled.connect(self.lennard_jones_params)
        self.radiobutton_carbondioxide.toggled.connect(self.lennard_jones_params)
        self.radiobutton_chloride.toggled.connect(self.lennard_jones_params)
        self.radiobutton_oxygen.toggled.connect(self.lennard_jones_params)
        self.radiobutton_nitrogen.toggled.connect(self.lennard_jones_params)
        self.radiobutton_krypton.toggled.connect(self.lennard_jones_params)
        self.radiobutton_xenon.toggled.connect(self.lennard_jones_params)
        self.radiobutton_other.toggled.connect(self.lennard_jones_params)
        self.thermostat_check.stateChanged.connect(self.thermostat_change)
        self.value_sigma.valueChanged.connect(self.lj_slider_connection_sig)
        self.value_sigma.valueChanged.connect(self.connect_red_density)
        self.value_epsilon.valueChanged.connect(self.lj_slider_connection_eps)
        self.value_epsilon.valueChanged.connect(self.reduce_temperature)
        self.epsilon_slider.valueChanged.connect(self.lj_value_connection_eps)
        self.sigma_slider.valueChanged.connect(self.lj_value_connection_sig)
        self.label_red_grav.setVisible(False)
        self.grav_red_unit.setVisible(False)
        self.temperature_change.valueChanged.connect(self.reduce_temperature)
        self.gravity_value.valueChanged.connect(self.grav_slider_connection)
        self.gravity_slide.valueChanged.connect(self.grav_value_connection)

    def enable_savingparameters(self, state):
        """enables groupbox for saving parameter options, when checkBox is true
        """
        if state > 0:
           self.groupbox_saveparameters.setEnabled(True)
        else:
           self.groupbox_saveparameters.setEnabled(False)

    def reduce_temperature(self):
        """contributes the temperature in reduced unit
        """
        self.temp_red_unit.setValue(float(self.temperature_change.value()/self.value_epsilon.value()))

    def contribute_boxlength(self):
        """contributes boxlength by given number of particles and density
        """
        volume = float(self.particles_np.value() / self.particles_density.value())
        box_l = str(round(volume**(1/3), 3))
        self.box_length_auto.setText(box_l+","+ box_l+","+ box_l)

    def connect_red_density(self):
        """when density is changed, reduced value is new contributed
        """
        self.red_density.setValue(max(0.001, self.particles_density.value()*(self.value_sigma.value()**3)))

    def connect_density(self):
        """when reduced density is changed, density value is new contributed
        """
        self.particles_density.setValue(self.red_density.value()*(self.value_sigma.value()**-3))


    def enable_gravityslider(self, state):
        """shows gravity slider and spinbox, when checkBox is true
        """
        if state > 0:
            self.gravity_slide.setVisible(True)
            self.gravity_value.setVisible(True)
            self.label_red_grav.setVisible(True)
            self.grav_red_unit.setVisible(True)
        else:
            self.gravity_slide.setVisible(False)
            self.gravity_value.setVisible(False)
            self.label_red_grav.setVisible(False)
            self.grav_red_unit.setVisible(False)

    def grav_value_connection(self):
        self.gravity_value.setValue(self.gravity_slide.value()*0.01)
        self.grav_red_unit.setValue(self.gravity_value.value()/2.477709)

    def grav_slider_connection(self):
        self.gravity_slide.setValue(int(self.gravity_value.value()*100))
        self.grav_red_unit.setValue(self.gravity_value.value()/2.477709)

    def lj_slider_connection_eps(self):
        """ changes value of slider, when epsilon value is changed
        """
        self.epsilon_slider.setValue(int(self.value_epsilon.value()))

    def lj_slider_connection_sig(self):
        """ changes value of slider, when sigma is changed
        """
        self.sigma_slider.setValue(int(100*self.value_sigma.value()))

    def lj_value_connection_eps(self):
        """ changes value of epsilon, when its slider is changed
        """
        self.value_epsilon.setValue(self.epsilon_slider.value())

    def lj_value_connection_sig(self):
        """ changes value of sigma, when its slider is changed
        """
        self.value_sigma.setValue(0.01*self.sigma_slider.value())

    def lennard_jones_params(self):

        self.epsilon_k = {'Argon':111.84,
                        'Chlorine':296.27,
                        'Carbon dioxide':201.71,
                        'Krypton':154.87,
                        'Oxygen':113.27,
                        'Nitrogen':91.85,
                        'Xenon':213.96} # epsilon/k_boltzmann in K
        self.sigma_ = {'Argon':0.3623,
                        'Chlorine':0.4485,
                        'Carbon dioxide':0.4444,
                        'Krypton':0.3895,
                        'Oxygen':0.3654,
                        'Nitrogen':0.3919,
                        'Xenon':0.4260} # sigma in nm

        if self.radiobutton_argon.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Argon'])
            self.value_sigma.setValue(self.sigma_['Argon'])

        if self.radiobutton_chloride.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Chlorine'])
            self.value_sigma.setValue(self.sigma_['Chlorine'])

        if self.radiobutton_carbondioxide.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Carbon dioxide'])
            self.value_sigma.setValue(self.sigma_['Carbon dioxide'])

        if self.radiobutton_krypton.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Krypton'])
            self.value_sigma.setValue(self.sigma_['Krypton'])

        if self.radiobutton_oxygen.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Oxygen'])
            self.value_sigma.setValue(self.sigma_['Oxygen'])

        if self.radiobutton_nitrogen.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Nitrogen'])
            self.value_sigma.setValue(self.sigma_['Nitrogen'])

        if self.radiobutton_xenon.isChecked():
            self.value_epsilon.setValue(self.epsilon_k['Xenon'])
            self.value_sigma.setValue(self.sigma_['Xenon'])

        if self.radiobutton_other.isChecked():
            self.value_epsilon.setEnabled(True)
            self.value_sigma.setEnabled(True)
            self.epsilon_slider.setEnabled(True)
            self.sigma_slider.setEnabled(True)
        else:
            self.value_epsilon.setEnabled(False)
            self.value_sigma.setEnabled(False)
            self.epsilon_slider.setEnabled(False)
            self.sigma_slider.setEnabled(False)

    def thermostat_change(self, state):
        """enables switching temperature, when thermostat is turned off
        """
        if state > 0:
            self.temperature_change.setVisible(True)
            self.temperature_slide.setVisible(True)
            self.temperature_slide.setEnabled(True)

        else:
            self.temperature_change.setVisible(False)
            self.temperature_slide.setEnabled(False)

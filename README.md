# The SimLearn

SimLearn is a set of tools to learn/teach about computer simulations. 

Stay tuned.
Explore and enjoy.

# Installation
```
pip3 install simlearn
```

# Packages

<center>
<img src="./assets/PyStarlogo.png" alt="PyStar Logo" width="400"/>
</center>

[PyStar](https://gitlab.com/alexander.d.kazakov/pystar/-/tree/master/application) an application for learning coarse-grained simulation using LJ potentials. Available: 2D/3D systems.

### Requirements
 * [ESPResSo package](https://espressomd.org/)

### QuickStart
```python
import espressomd
from simlearn import PyStar
PyStar.run()
```

# The team
SimLearn is currently maintained by [Alexander D. Kazakov](https://github.com/AlexanderDKazakov/) with contributions coming from talented individuals in various forms and means. A non-exhaustive but growing list needs to mention: Tabea G. Langen, Pascal Hebbeker, Peter Kosovan, Filip Uhlík, Lucie Nová.
